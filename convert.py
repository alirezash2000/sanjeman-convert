import pdfkit
import time
print("=======================")

start_time = time.time() #تنظیم یک تایمر برای بدست آوردن زمان صرف شده
def save_pdf(url, output_path, wkhtmltopdf_path):
    
    options = {        #تنظیمات صفحه فایل pdf
        'quiet': ''
    }
    pdfkit.from_url(url, output_path, options=options, configuration=pdfkit.configuration(wkhtmltopdf=wkhtmltopdf_path))


url = "https://en.wikipedia.org/wiki/iran" # آدرس صفحه سایتی که میخواهیم تبدیل کنیم
output_path = r"C:\Users\Lenovo\Desktop\sanjeman\example.pdf"             #آدرس جایی که فایل در آن ذخیره شود
wkhtmltopdf_path = r"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe"  #    در آن قرار دارد wkhtmltopdf آدرسی که 
save_pdf(url, output_path, wkhtmltopdf_path)



print ("job Done and PDF Format File is Saved ::)))))")


#محاسبه زمان صرف شده
end_time = time.time()
execution_time = end_time - start_time
print("=======================")
print("TIMER:")
print(f"Execution time: {execution_time:.2f} seconds")

print("=======================")